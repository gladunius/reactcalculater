import React, {useCallback, useMemo} from "react";
import {logDOM} from "@testing-library/react";

export const useCalculator = () => {
    const [numbers, setNumbers] = React.useState([''])
    const [operators, setOperators] = React.useState([])

    const [calculationHistory, setCalculationHistory] = React.useState("")
    const [inputEnabled, setInputEnabled] = React.useState(false);
    const displayValue = React.useMemo(() => {

            return numbers.map((number, index) => {
                return numbers[index] + ' ' + (operators[index] || '')
            }).join(" ")
        }, [numbers, operators]
    )


    const enterNumber = (number) => setNumbers((numbers) => {

        let newNumbers = [...numbers]
        newNumbers[newNumbers.length - 1] += number
        setInputEnabled(true)
        console.log("Ввод цифры")

        return newNumbers
    })


    const enterOperator = (operator) => {
        console.log("Ввод символа")
        if (inputEnabled === true) {
            if (operator === '.') {
                let pointСheck = numbers[numbers.length - 1].includes(".")
                if (!pointСheck) {
                    let newNumbers = [...numbers]
                    newNumbers[newNumbers.length - 1] += operator
                    setNumbers(newNumbers)
                    console.log(numbers)
                    setInputEnabled(false)
                }
            } else {

                console.log(displayValue.replace(/\s+/g, '')[0])
                setOperators([...operators, operator])
                setNumbers(nums => [...nums, ""]);
                setInputEnabled(false)
            }

        }
    }


    const reset = () => {
        setNumbers([''])
        setOperators([])
    }
    const remove = () => {
        let newDisplayValue = displayValue.replace(/ /g, "")
        console.log(!isNaN(newDisplayValue[newDisplayValue.length - 1]) || newDisplayValue[newDisplayValue.length - 1] === ".")
        if (!isNaN(newDisplayValue[newDisplayValue.length - 1]) || newDisplayValue[newDisplayValue.length - 1] === ".") {
            setNumbers(removeNumbers => {
                const newNumbers = [...removeNumbers];
                const lastElement = newNumbers[newNumbers.length - 1];

                if (lastElement.length > 0) {
                    newNumbers[newNumbers.length - 1] = lastElement.slice(0, -1);

                    if (newNumbers[newNumbers.length - 1].length === 0) {
                        newNumbers.pop();
                    }
                } else {
                    newNumbers.pop();
                }

                return newNumbers;
            });
        } else {
            setOperators(removeOperator => {
                let newOperator = [...removeOperator]
                newOperator.pop()
                return newOperator
            })
        }
    }
    const signChange = () => {

    }
    const calculate = () => {
        let result = eval(displayValue.replace(/x/g, "*"))
        setNumbers([result.toFixed(6).replace(/\.?0*$/, '')])
        setOperators([])
    }


    return {
        enterNumber,
        enterOperator,
        reset,
        remove,
        numbers,
        operators,
        calculationHistory,
        setCalculationHistory,
        signChange,
        calculate,
        displayValue
    };
}