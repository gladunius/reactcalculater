import "./styles/App.css"
import Header from "./components/Header/Header";
import React from "react";
import {cn} from "./utils";
import Display from "./components/Display/Display";
import ButtonBar from "./components/ButtonBar/ButtonBar";
import {useCalculator} from "./useCalculator"

export const ThemeContext = React.createContext({});

function App() {
    const [theme, setTheme] = React.useState("light")

    const calculator = useCalculator()
    return <div className={cn("App", theme)}>
            <ThemeContext.Provider value={{theme, setTheme}}>
                <Header/>
                <Display calculator={calculator}/>
                <ButtonBar calculator={calculator}/>
            </ThemeContext.Provider>
        </div>


}

export default App;