import React from 'react';
import classes from "./Header.module.css"
import ModeButton from "../ModeButton/ModeButton";
import {ThemeContext} from "../../App";
import imageMenu from "../../images/menu.svg"
import imageMoon from "../../images/dark_mode.svg"
import imageSun from "../../images/light_mode.svg"
import {cn} from "../../utils";

const Header = () => {
    const themeContext = React.useContext(ThemeContext);
    const changeTheme = () => {
        themeContext.setTheme(themeContext.theme === "light" ? "dark" : "light");
    }
    return <div className={cn(classes.header, classes[themeContext.theme])}>
        <img className={classes.imageMenu} src={imageMenu} alt=""/>
        <ModeButton value={"Calculator"}/>
        <ModeButton value={"Converter"}/>
        {<img onClick={changeTheme} className={classes.imageMenu}
              src={themeContext.theme === "light" ? imageMoon : imageSun} alt=""/>}
    </div>
};

export default Header;