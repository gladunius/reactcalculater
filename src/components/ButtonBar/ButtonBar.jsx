import React from 'react';
import classes from "./ButtonBar.module.css"
import {Button} from "../ButtonControl/Button"
import {ThemeContext} from "../../App";
import {useControls} from "../../useControls";


const ButtonBar = (props) => {
    const themeContext = React.useContext(ThemeContext);
    const listButton = useControls(themeContext.theme, props.calculator)

    return <div className={classes.ButtonBar}>
        {listButton.map((button, index) => <Button key={index} {...button}/>)}
    </div>
};

export default ButtonBar