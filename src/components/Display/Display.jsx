import React from 'react';
import classes from "./Display.module.css";
import {cn} from "../../utils";
import {ThemeContext} from "../../App";


const Display = (props) => {
    const themeContext = React.useContext(ThemeContext);
    return <div className={classes.display}>
        <div className={cn(classes.calculations)}>
            {props.calculator.calculationHistory}
        </div>
        <div className={cn(classes.inputNumbers, classes[themeContext.theme])}>
            {props.calculator.displayValue}
        </div>
    </div>

};

export default Display;