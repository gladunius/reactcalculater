import React from 'react';
import classes from "./ButtonControl.module.css"
import {cn} from "../../utils";

export const Button = (props) => {
    return <button onClick={props.onClick} className={cn(classes.Button,
                classes[`button--bg-${props.backgroundColor}`])}>
            {props.icon? <img className={cn(classes.icon, classes[`icon--color-${props.iconColor}`])}
                src={props.icon} alt=""/>:
                <div className={classes[`text--color-${props.textColor}`]}>{props.value}</div>
                 }
        </button>;
};
