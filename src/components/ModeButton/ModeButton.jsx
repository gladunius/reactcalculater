import React from 'react';
import classes from "./ModeButton.module.css"
import {ThemeContext} from "../../App";
import {cn} from "../../utils";

const ModeButton = (props) => {
    const themeContext = React.useContext(ThemeContext);

    return <button className={cn(classes.myButton, classes[themeContext.theme])}>
        {props.value}
    </button>
};

export default ModeButton;