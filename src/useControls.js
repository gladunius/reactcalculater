import React, {useMemo} from "react";
import iconBackspace from "./images/backspace.svg";
import iconPercent from "./images/percent.svg";
import iconDivision from "./images/division.svg";
import iconMultiplication from "./images/multiplication.svg";
import iconMinus from "./images/minus.svg";
import iconPlus from "./images/plus.svg";
import iconEquals from "./images/equals.svg";

export const useControls = (theme, calculator) => [
        {
            value: "AC", backgroundColor: theme === "dark" ? "lightGreen" : "white",
            textColor: theme === "dark" ? "white" : "gray",
            onClick: () => calculator.reset()
        },
        {
            value: "removal", backgroundColor: theme === "dark" ? "lightGreen" : "white",
            iconColor: theme === "dark" ? "lightGray" : "gray", icon: iconBackspace,
            onClick: () => calculator.remove("removal")
        },
        {
            value: "%", style: "operation", icon: iconPercent,
            backgroundColor: "white",
            onClick: () => calculator.enterOperator("%")
        },
        {
            value: "/", style: "operation", icon: iconDivision, iconColor: "white",
            backgroundColor: "white",
            onClick: () => calculator.enterOperator("/")
        },
        {
            value: "7", backgroundColor: theme === "dark" ? "darkGreen" : "white",
            textColor: theme === "dark" ? "white" : "gray",
            onClick: () => calculator.enterNumber("7")
        },
        {
            value: "8", backgroundColor: theme === "dark" ? "darkGreen" : "white",
            textColor: theme === "dark" ? "white" : "gray",
            onClick: () => calculator.enterNumber("8")
        },
        {
            value: "9", backgroundColor: theme === "dark" ? "darkGreen" : "white",
            textColor: theme === "dark" ? "white" : "gray",
            onClick: () => calculator.enterNumber("9")
        },
        {
            value: "x", icon: iconMultiplication,
            backgroundColor: "white",
            onClick: () => calculator.enterOperator("x")
        },
        {
            value: "4", backgroundColor: theme === "dark" ? "darkGreen" : "white",
            textColor: theme === "dark" ? "white" : "gray",
            onClick: () => calculator.enterNumber("4")
        },
        {
            value: "5", backgroundColor: theme === "dark" ? "darkGreen" : "white",
            textColor: theme === "dark" ? "white" : "gray",
            onClick: () => calculator.enterNumber("5")
        },
        {
            value: "6", backgroundColor: theme === "dark" ? "darkGreen" : "white",
            textColor: theme === "dark" ? "white" : "gray",
            onClick: () => calculator.enterNumber("6")
        },
        {
            value: "-", icon: iconMinus,
            backgroundColor: "white",
            onClick: () => calculator.enterOperator("-")
        },
        {
            value: "1", backgroundColor: theme === "dark" ? "darkGreen" : "white",
            textColor: theme === "dark" ? "white" : "gray",
            onClick: () => calculator.enterNumber("1")
        },
        {
            value: "2", backgroundColor: theme === "dark" ? "darkGreen" : "white",
            textColor: theme === "dark" ? "white" : "gray",
            onClick: () => calculator.enterNumber("2")
        },
        {
            value: "3", backgroundColor: theme === "dark" ? "darkGreen" : "white",
            textColor: theme === "dark" ? "white" : "gray",
            onClick: () => calculator.enterNumber("3")
        },
        {
            value: "+", icon: iconPlus,
            backgroundColor: "white",
            onClick: () => calculator.enterOperator("+")
        },
        {
            value: "+-", backgroundColor: theme === "dark" ? "darkGreen" : "white",
            textColor: theme === "dark" ? "white" : "gray",
            onClick: () => calculator.signChange()
        },
        {
            value: "0", backgroundColor: theme === "dark" ? "darkGreen" : "white",
            textColor: theme === "dark" ? "white" : "gray",
            onClick: () => calculator.enterNumber("0")
        },
        {
            value: ".", backgroundColor: theme === "dark" ? "darkGreen" : "white",
            textColor: theme === "dark" ? "white" : "gray",
            onClick: () => calculator.enterOperator(".")
        },
        {
            value: "=", icon: iconEquals, backgroundColor: "white",
            onClick: () => calculator.calculate()
        },
    ]
